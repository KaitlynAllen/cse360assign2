/**
 * Author: Kaitlyn Allen
 * Class ID: 378
 * Assignment #2
 * 
 * Description: Creates a SimpleList class which is an array object. 
 * Several functions can be performed on the array. Elements can be 
 * added to, removed from, counted in, and searched the array.
 */

package assign2;

import java.util.*;

public class SimpleList {
	
	private int list[];
	private int newList[];
	private int count;
	private int doubleSize = 10;

	
	/**
	 * Class constructor
	 */
	public SimpleList() {
		list = new int[10];
		count = 0;
		for(int i =0; i<10;i++) {
			list[i] = -10000;
		}
	}
	
	
	/**
	 * Adds integer element to the SimpleList structure. If the SimpleList
	 * is not full, the input integer is added to the front of the
	 * SimpleList and all other elements are moved to the right one place to
	 * keep the list in order. 
	 * If the SimpleList is full, the input integer is 
	 * placed at the front of the list and all of the elements are moved to the 
	 * right one place, and the last element is deleted from the list to make room
	 * for the new element.
	 * 
	 * @param num the number that is to be added
	 */
	public void add(int num) {
		count++;
		if(count == 1) {
			list[0] = num;
		}
		
		//moves elements to the right so inserted element is placed at front of list
		else if(count > 1 && count <= doubleSize){
			for(int place = doubleSize-1; place > 0; place--) {
				list[place] = list[place-1];
			}
			list[0] = num;

		}
		//drops last element if array is full to make room for new element
		else {
			doubleSize = (int) (count + Math.floor(count/2));
			newList = new int[doubleSize];
			newList[0] = num;
			for(int i = 1; i <= list.length; i++) {
				newList[i] = list[i-1];
			}
			list = newList;
		}
	}
	
	
	/**
	 * Removes element from SimpleList and moves remaining
	 * numbers to the left after removal
	 * 
	 * @param num the integer that is to be removed
	 */
	public void remove(int num) {
		int found = search(num);
		int track = 0;
		if(found != -1) {
        for (int place = 0; place < doubleSize; place++)
           if (list[place] != num) {
              list[track++] = list[place];
           }
           else
        	   count--;
		}
		
		 int emptySpace = doubleSize-count;
		 double quota = doubleSize*.25;
		if(emptySpace > quota && count>= 1) {
			doubleSize = (int) (count + Math.floor(quota));
			newList = new int[doubleSize];
			for(int i = 0; i < doubleSize; i++) {
				newList[i] = list[i];
			}
			list = newList;
		}
	}
	
	/**
	 * Returns the number of integers in the SimpleList.
	 * @return the count of integers
	 */
	public int count() {
		return count;
	}
	
	
	/**
	 * Returns the elements in SimpleList as a string of integers.
	 */
	public String toString() {
		String arrayPrint= "";
		if(count == 0)
			return arrayPrint;
		else {
		for(int i = 0; i < count-1; i++) {
			if(list[i]!= -10000)
			arrayPrint += list[i] + " ";
		}
		return arrayPrint+= list[count-1];
		}
	}
	
	
	/**
	 * Searches for element in SimpleList and returns index, 
	 * if element doesn't exist, returns -1.
	 * 
	 * @param num the integer that is being searched for
	 * @return either the position of the integer in SimpleList or negative
	 */
	public int search(int num) {
		int place =0;
		while(place < list.length) {
			if (list[place] == num) 
				return place;
			else
				place++;
		}
			return -1;
	}
	
	/**
	 * Adds a number to the end of the SimpleList and increase the
	 * size of the list if the list is already full to accomodate
	 * the new integer.
	 * 
	 * @param num the number to add to the end of the SimpleList
	 */
	public void append(int num) {
		count++;
		if(count <= doubleSize) {
			list[list.length-1] = num;
		}
		else {
			doubleSize = (int) (count + Math.floor(count/2));
			newList = new int[doubleSize];
			newList[list.length] = num;
			for(int i = 0; i < list.length; i++) {
				newList[i] = list[i];
			}
			list = newList;
		}
	}
	
	/**
	 * Returns the first number in the SimpleList
	 * @return
	 */
	public int first() {
		if(count == 0)
			return -1;
		else
			return list[0];
	}
	
	/**
	 * Returns the last number in the SimpleList
	 * @return
	 */
	public int last() {
		if(count==0)
			return -1;
		else
			return list[count-1];
	}
	
	/**
	 * Returns the current number of possible locations in
	 * the SimpleList
	 * 
	 * @return
	 */
	public int size() {
		return doubleSize;
	}
	

}//end of class



